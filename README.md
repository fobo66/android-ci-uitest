# Android CI

This is a lightweight [Docker](https://www.docker.com/) image for running Android instrumented tests on [GitLab CI](https://about.gitlab.com/solutions/continuous-integration/). It is based on [Debian 12 Slim](https://debian.org/) and has only the most recent versions of SDKs. Heavily inspired by [this image](https://github.com/jangrewe/gitlab-ci-android)

Contents:

 * [Ruby](https://www.ruby-lang.org/en/) (+ [Bundler](https://bundler.io/)) for running [Fastlane](https://fastlane.tools/) or [Danger](https://danger.systems/ruby/)
 * [Git](https://git-scm.com/)
 * [Curl](https://curl.se/)
 * [Android SDK 34 and 35](https://developer.android.com/tools/releases/platforms)
 * [Android Build tools 34 and 35](https://developer.android.com/tools/releases/build-tools)
 * [Google Play services](https://developers.google.com/android)
 * [Android SDK 35 AOSP ATD image](https://developer.android.com/studio/test/gradle-managed-devices#gmd-atd)

## Getting started

To use this image, add this line to the top of your `.gitlab-ci.yml` or to some particular job inside it:

```yml
image: registry.gitlab.com/fobo66/android-ci-uitest
```

## Contributions

Contributions are always welcome, feel free to set up an MR and make sure it works.
